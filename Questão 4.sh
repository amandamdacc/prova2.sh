#/bin/bash

original_ps1=$PS1


echo "Opções disponíveis:"
echo "1. Padrão"
echo "2. Verde em negrito"
echo "3. Azul em negrito"
echo "4. Vermelho em negrito"

echo -n "Escolha uma opção para customizar o prompt: "
read escolha

PS1=$([ "escolha" == "1"] && echo '\h:\W \u\$ ' ||
      [ "$escolha" == "2" ] && echo '\[\033[1;32m\]\h:\W \u\$ \[\033[0m\]' ||
      [ "$escolha" == "3" ] && echo '\[\033[1;34m\]\h:\W \u\$ \[\033[0m\]' ||
      [ "$escolha" == "4" ] && echo '\[\033[1;31m\]\h:\W \u\$ \[\033[0m\]' ||
      echo 'Escolha inválida.'
)

echo "PS1 configurada para: $PS1"

read -p "Pressione ENTER para restaurar a configuração" orinal.

PS1=$original_ps1

echo "O prompt foi restaurado para: $PS1"
